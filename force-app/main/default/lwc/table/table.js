import { LightningElement, track } from 'lwc';



export default class Table extends LightningElement {

    keyIndex = 0;
    @track accountList = [];
    get options() {
        return [
            { label: 'Male', value: 'Male' },
            { label: 'Female', value: 'Female' },
        ];
    }

    addNewRow(event) {
        //console.log('Account List Length '+this.accountList.length);
        //console.log('Save List Length: '+this.accountSaveList.length);
        ++this.keyIndex;
        var newItem = [{ id: this.keyIndex, Name: '', Gender: '', Designation: '', FullTime: '', Salary: '', Email: '', Description: '', edit: true }];
        this.accountList = this.accountList.concat(newItem);
        let addRowValues = JSON.parse(JSON.stringify(this.accountList))
        console.log(addRowValues);
        var indx = this.accountList.length;
        let indexx = event.target.dataset.value;
        //console.log("INDEX: "+indexx)
        //console.log('Length:: '+indx);
    }



    //-------------SAVE ------------------------//

    @track name;
    @track submit;
    @track gender;
    @track designation;
    @track fulltime;
    @track salary;
    @track email;
    @track description;

    onNameHandle(event) {
        let accessK = event.target.dataset.id;
        console.log(accessK);
        this.accountList.forEach(ele => {
            if (ele.id == accessK) { ele.Name = event.target.value; }
        })
        // this.name = event.target.value;
        // console.log(this.name);
    }
    onGenderHandle(event) {
        let accessK = event.target.dataset.id;
        console.log(accessK);
        this.accountList.forEach(ele => {
            if (ele.id == accessK) { ele.Gender = event.target.value; }
        })
        //this.gender = event.target.value;
        //console.log( this.gender);
    }
    onDesignationHandle(event) {
        let accessK = event.target.dataset.id;
        console.log(accessK);
        this.accountList.forEach(ele => {
            if (ele.id == accessK) { ele.Designation = event.target.value; }
        })
        // this.designation = event.target.value;
        // console.log(this.designation);
    }
    onFullTimeHandle(event) {
        let accessK = event.target.dataset.id;
        console.log(accessK);
        this.accountList.forEach(ele => {
            if (ele.id == accessK) {
                if (event.target.checked == true) {
                    ele.fulltime = event.target.checked;
                }
                else {
                    ele.fulltime = event.target.checked;
                }


            }
        })

    }
    onSalaryHandle(event) {
        let accessK = event.target.dataset.id;
        console.log(accessK);
        this.accountList.forEach(ele => {
            if (ele.id == accessK) { ele.Salary = event.target.value; }
        })

    }
    onEmailHandle(event) {
        let accessK = event.target.dataset.id;
        console.log(accessK);
        this.accountList.forEach(ele => {
            if (ele.id == accessK) { ele.Email = event.target.value; }
        })

    }
    onDescriptionHandle(event) {
        let accessK = event.target.dataset.id;
        console.log(accessK);
        this.accountList.forEach(ele => {
            if (ele.id == accessK) { ele.Description = event.target.value; }
        })
    }

    saveRow(event) {

        let accessK = event.target.accessKey;
        console.log(accessK);
        this.accountList.forEach(ele => {
            if (ele.id == accessK) { ele.edit = false; }
        })
        // if( (this.name   && this.gender && this.designation  && this.salary && this.email && this.description))
        // {
        //   var newId = this.accountSaveList.length + 1 ;
        //   console.log('INDX: '+newId );
        //   //var saveItem = [{ id: indx, Name: this.name, Gender: this.gender, Designation: this.designation, FullTime: this.fulltime, Salary: this.salary, Email: this.email, Description: this.description, Action: 'TEST' }];
        //   let saveItem = { id: newId, Name: this.name, Gender: this.gender, Designation: this.designation, FullTime: this.fulltime, Salary: this.salary, Email: this.email, Description: this.description, Action: 'TEST' };

        //   this.accountSaveList = this.accountSaveList.concat(saveItem);
        //   let saverow = JSON.parse(JSON.stringify(this.accountSaveList))
        //   console.log(saverow);


        /*console.log("Name: "+this.name);
        console.log("Gender: "+this.gender);
        console.log("Designation: "+this.designation);
        console.log("Salary: "+this.salary);
        console.log("Email: "+this.email);
        console.log("Description: "+this.description);*/

        // }
        // else{
        //     alert("Enter all fields");
        // }
    }

    removeRow(event) {
        if (this.accountList.length >= 1) {
            //console.log(event.target.accessKey+"event.target.accesskey");
            this.accountList = this.accountList.filter(function (element) {
                //console.log(parseInt(element.id)+"parseelement");
                //console.log("parseInt==" +parseInt(event.target.accessKey));
                return parseInt(element.id) !== parseInt(event.target.accessKey);
            });
        }
    }
    editRow(event) {

        var currentRow = event.target.accessKey;
        for (let i = 0; i < this.accountList.length; i++) {
            if (this.accountList[i].id == currentRow) {
                this.accountList[i].edit = true;
                this.accountList[i].Name.value = event.target.value;
                this.accountList[i].Gender.value = event.target.value;
                this.accountList[i].Designation.value = event.target.value;
                this.accountList[i].FullTime.value = event.target.checked;
                this.accountList[i].Salary.value = event.target.value;
                this.accountList[i].Email.value = event.target.value;
                this.accountList[i].Description.value = event.target.value;


            }
        }

    }

}