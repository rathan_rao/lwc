import { LightningElement, track } from 'lwc';

const columns = [
    { label: 'Name', fieldName: 'name' },
    { label: 'Gender', fieldName: 'gender', type: 'radio' },
    { label: 'Designation', fieldName: 'designation', type: 'select' },
    { label: 'Full Time', fieldName: 'fullTime', type: 'checkbox' },
    { label: 'Salary', fieldName: 'salary', type: 'number' },
    { label: 'Email', fieldName: 'email', type: 'email' },
    { label: 'Description', fieldName: 'description', type: 'text' },
    { label: 'Action', fieldName: 'description', type: 'text' },
    
];


export default class Table_task extends LightningElement {

    @track accountList =[];
    i = 0;
    columns = columns;

    connectedCallback() 
    {
       //this.row();
    }
    addRow()
    { 
        ++this.i;
        var indx = this.accountList.length + 1 ;
        let data1 = { id: indx, name:'Rathan' + this.i, 
                        'gender':'Male',
                        'designation':'HR',
                        'fullTime':'yes', 
                        'salary':'12121', 
                        'email':'sdsd@gmail.com','description':'hello' };
        console.log(data1);
        //accountList.push(data1);
        this.accountList = this.accountList.concat(data1);
        let cases = JSON.parse(JSON.stringify(this.accountList))
        console.log(cases);
    }
    row()
    {
        this.addRow(this.accountList);
    }
  
}