import { LightningElement,track, wire } from 'lwc';
import accountList from "@salesforce/apex/accountHandler.accountList";

export default class TaskOne extends LightningElement {

    @track accountList;
    @track searchNewKey;

    @wire(accountList, {searchKey : "$searchNewKey"})
    acccountHandler({data,error})
    {
        if(data)
        {
            console.log(JSON.parse(JSON.stringify(data)));
            this.accountList = data;
        }
        else if(error)
        {
            console.error(error);
        }
    }
    response()
    {
        return this.accountList ? true : false; 
    }
    
    searchKeyHandler(event)
    {
        this.searchNewKey = event.target.value;
        console.log("SEARCH KEY"+this.searchNewKey);;
    }


    }

