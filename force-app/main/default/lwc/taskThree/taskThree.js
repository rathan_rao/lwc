import { LightningElement, track } from 'lwc';
import insertAccount from "@salesforce/apex/accountHandler.insertAccount";


export default class TaskThree extends LightningElement {

    @track objAccount ={};
    @track accountId; 


    insertAccountHandler()
    {
        insertAccount({objAccount : this.objAccount}).then(result =>{
            this.accountId = result;

        }).catch( error =>{
            console.error(error);
        })

    }

    createRecord()
    {
        this.insertAccountHandler();
    }

    inputChangeHandler(event)
    {
        let fieldName = event.target.name;
        console.log("Name: "+fieldName);
        let val = event.target.value;
        console.log("Value: "+val);
        this.objAccount[fieldName] = val;
        console.log("Final Value: "+this.objAccount[fieldName]);

        
    }
    addressHandler(event)
    {
        let addressName = event.target.name;
        console.log('Address Name: '+addressName);
        let addressStreet = event.target.street;
        console.log('Street Name: '+addressStreet);
        let addressCity = event.target.city;
        console.log('City Name: '+addressCity);
        
        this.objAccount.Street = event.target.street;
        this.objAccount.city = event.target.city;
        this.objAccount.postal = event.target.postalCode;
        this.objAccount.state = event.target.province;
        this.objAccount.country = event.target.country;
        console.log( JSON.parse(JSON.stringify(this.objAccount)));




    }
    
    

    



}