import { LightningElement,wire,track } from 'lwc';
import accountListTwo from "@salesforce/apex/accountHandler.accountListTwo";

export default class TaskTwo extends LightningElement {

    @track accountListNew;
    @track key;

    accountHandler()
    {
        accountListTwo({searchNewKey : this.key}).then( result =>{
            this.accountListNew = result;
        }).catch( error =>{
            console.error(error);
        })
        
    }
    onButtonClick()
    {
        this.accountHandler();
    }
    responseButton()
    {
        return this.accountListNew ? true: false;
    }

    searchKey(event)
    {
        this.key = event.target.value;
    }

    

}